//AUTHOR:   Daniel Ruffolo
//
//DESCRPTION:
//A javascript Datastructure running in node.js
//
//the fllowing algorithm takes in a array as a 
//argument and perormes the mmerge sort algorithm in order to sorth the values

function mergeSort(array) {
    if (array.length < 2) return array;

    var middleIndex = Math.floor(array.length / 2);
    var firstHalf = array.slice(0, middleIndex);
    var secondHalf = array.slice(middleIndex);

    return merge(mergeSort(firstHalf), mergeSort(secondHalf));

}

function merge (array1, array2){
    var result = [];
    while (array1.length && array2.length) {
        var minElm;
        if (array1[0] < array2[0]) minElm = array1.shift();
        result.push(minElm);

    }

    if (array1.length) result = rsult.concat(array1);
    else result = result.concat(array2);
    return result;
}

console.log( mergeSort([6000, 34, 203, 3]));
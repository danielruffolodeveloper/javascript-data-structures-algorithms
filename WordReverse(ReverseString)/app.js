//AUTHOR:   Daniel Ruffolo
//
//DESCRPTION:
//A javascript Datastructure running in node.js
//
//the fllowing algorithm takes in a array as a 
//argument and traverses the aray reversing the array 
//elements in reverse order. this effectvely scrambles te string

function reverseWords(string){
    var wordsArray = string.split(' ');
    var reversedWordsArr = [];

    wordsArray.forEach(word => {
        var reversedWord = '';
        for (var i = word.length - 1; i >= 0; i--) {
            reversedWord += word[i];

    }
    reversedWordsArr.push(reversedWord);
    });
    
    return reversedWordsArr.join(' ');
}

console.log(reverseWords('this is a simple string of text'))
//AUTHOR:   Daniel Ruffolo
//
//DESCRPTION:
//A javascript Datastructure running in node.js
//
//the fllowing algorithm takes in a array as a 
//argument and traverses the aray reversing the array 
//elements from highest to lowest

function reverseArray(array) {
    for (var i = 0; i < array.length / 2; i ++) {

        var temp = array[i];
        array[i] = array[array.length - 1 - i];
        array [array.length - 1 - i] = temp;

    }
    return array;
}
console.log(reverseArray([1, 2, 3, 4, 5, 6]))
